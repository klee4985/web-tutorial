//initializing settings for Royal Slider plugin
jQuery(document).ready(function($) {
	$(".royalSlider").royalSlider({
		keyboardNavEnabled: false,
		autoPlay: {
			// autoplay options go gere
			enabled: true,
			pauseOnHover: true,
			delay: 10000
		},
		imageScalePadding: 0,
		slidesSpacing: 0
	});  
});
//---------------------------------

//LOGIN & REGISTER MODAL POPUP
$(document).ready(function() {

	$('a.login-window, a.register-window').click(function() {
		
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');

		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').click(function() { 
	  $('#mask , .login-popup, .register-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
});
//---------------------------------

//GALLERY-------------------------

//spacing between thumbnails
var thumbnailSpacing = 10;

$(document).ready(function() {
	
	//add and remove css class from sort links depending on the click
	$('li.sortLink').on('click', function(e){
		e.preventDefault();
		$('li.sortLink').removeClass('selected');
		$(this).addClass('selected');
		var keyword = $(this).attr('data-keyword');
		sortThumbnails(keyword);
	});
	
	$('li.category').on('click', function(e){
		e.preventDefault();
		$('li').removeClass('selected');
	});
	
	$('.thumbnail_container a.thumbnail').addClass('showMe').addClass('fancybox').attr('rel','group');
		positionThumbnails();

});

//sort the thumbnails according to the category (keyword)
function sortThumbnails(keyword){
	$('.thumbnail_container a.thumbnail').each(function(){
		
		var thumbnailKeywords = $(this).attr('data-keywords');
		
		if(keyword == 'all'){
			$(this).addClass('showMe').removeClass('hideMe').attr('rel','group');;
		}else{
			if(thumbnailKeywords.indexOf(keyword) != -1){
				$(this).addClass('showMe').removeClass('hideMe').attr('rel','group');;
			}else{
				$(this).addClass('hideMe').removeClass('showMe').attr('rel','none');;
			}
		}
	});
	
	//call positionThumbnails after thumbnails have been sorted 
	positionThumbnails();

};

//position thumbnails according to available div size
function positionThumbnails(){
	
	$('.thumbnail_container a.thumbnail.hideMe').animate({opacity:0},500,function(){
//		$(this).css({'display':'none','top':'0px','left':'0px'});
		
	});
	
	
	var containerWidth = $('.photos').width();
	var thumbnail_R = 0;
	var thumbnail_C = 0;
	var thumbnailWidth = $('a.thumbnail img:first-child').outerWidth() + window.thumbnailSpacing;
	var thumbnailHeight = $('a.thumbnail img:first-child').outerHeight() + window.thumbnailSpacing;
	var max_C = Math.floor(containerWidth / thumbnailWidth);
	
	$('.thumbnail_container a.thumbnail.showMe').each(function(index){
		var remainder = (index%max_C)/100;
		var maxIndex = 0;
		
		if(remainder == 0){
			if(index != 0){
				thumbnail_R += thumbnailHeight;
			}
			thumbnail_C = 0;
		}else{
			thumbnail_C += thumbnailWidth;
		}
		
		$(this).css('display','block').animate({
			'opacity':1,
			'top':thumbnail_R+'px',
			'left':thumbnail_C+'px'
		}, 500);
		
		var newWidth = max_C * thumbnailWidth;
		var newHeight = thumbnail_R + thumbnailHeight;
		$('.thumbnail_container').css({'width':newWidth+'px','height':newHeight+'px'});
	
	});
	
	
	detectFancyboxLinks();
	
}

//initializing settings for FancyBox plugin
function detectFancyboxLinks(){
	$('a.fancybox[rel="group"]').fancybox({
		'transitionIn' : 'elastic',
		'transitionOut': 'elastic',
		'titlePosition' : 'over',
		'speedIn' : 500,
		'overlayColor' : '#000',
		'padding' : 0,
		'overlayOpacity' : .75
	});
}
//--------------------------------------

//comment
function validateForm()
{
	if (document.forms["myForm"]["title"].value != "") {
		var x=document.forms["myForm"]["title"].value;
		var newText = document.createTextNode(x);
		var newMsg = document.createElement("li");
		newMsg.appendChild(newText);
		var docBody = document.getElementsByTagName("ul")[3];
		docBody.appendChild(newMsg);
		document.getElementById("postNewComment").reset();
	}
}