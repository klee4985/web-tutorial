$(document).ready(function() {
	$("form#RegisterUserForm").submit(function(e) {
		// Make sure you suppress the default browser submit action, otherwise you will not be able to send the data
		e.preventDefault();
		
		// Extract out all the data in the form, and translate it into a GET method string to pass to the server
		var dataPass = $("form#RegisterUserForm").serialize();
		// So you can see the data that is passed to the server, let's log it into the console
		console.log(dataPass);

		$.ajax({
			url : "http://www.ug.it.usyd.edu.au/~i13s133/server/register.cgi",
			type : "GET",
			data : dataPass,
			success : function() {
				alert("You successfully created an account");
				$('#mask , .register-popup').fadeOut(300 , function() {
					$('#mask').remove();
				});
				 window.location.replace("http://www.ug.it.usyd.edu.au/~i13s133/html/profile.html");
			}
		});
	});
});

$(document).ready(function() {
	$("form#loginUserForm").submit(function(e) {
		e.preventDefault();

		var dataPass = $("form#loginUserForm").serialize();
		console.log(dataPass);
		$.ajax({
			url : "server/login.cgi",
			type : "GET",
			data : dataPass,
			success : function(data) {
				//data  = JSON.stringify(data);
				//data = $.trim(data);
				console.log(data);

				if (data === "success\n") {
					alert("You've successfully logged in");
					 $('#mask , .login-popup').fadeOut(300 , function() {
					 	$('#mask').remove();
					 });
					 window.location.replace("http://www.ug.it.usyd.edu.au/~i13s133/html/profile.html");
				}
				else {
					alert("Invalid username or password");
				}
			}
		});
	});
});

