import psycopg2
import psycopg2.extras
import sys
import os
import cgi
import cgitb
import hashlib

cgitb.enable()
print 'Content-Type: text/plain\r\n\r'

try:
    conn = psycopg2.connect("dbname='info1003_project' user='info1003user' host='postgres.it.usyd.edu.au' password='userpj1003'")
    # Parsing form data
    urlString = "?" + os.environ['QUERY_STRING']
    #urlString = "?username=pikachu&passd=yellowMonster"
    from urlparse import urlparse, parse_qs
    qs = urlparse(urlString).query
    dataJson = parse_qs(qs)
    
    # INSERTING username and password into users table
    userName = dataJson.get('username')[0]
    passd1 = hashlib.sha256(dataJson.get('passd')[0]).hexdigest()
    
    cursor = conn.cursor()
    cursor.execute("SELECT passwd FROM users WHERE username = '" + userName + "'")
    
    records = cursor.fetchall()

    if len(records) == 1:
        if records[0][0] == passd1:
            print "success"
        else:
            print "invalid"
    else:
        print "invalid"
except:
    # Get the most recent exception
    exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
    # Exit the script and print an error telling what happened.
    sys.exit("Database connection failed!\n ->%s" % (exceptionValue))
