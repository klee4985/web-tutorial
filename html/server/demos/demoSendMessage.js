$(document).ready(function() {
	$("form#exampleForm").submit(function(e) {
		// Make sure you suppress the default browser submit action, otherwise you will not be able to send the data
		e.preventDefault();

		// Extract out all the data in the form, and translate it into a GET method string to pass to the server
		var messSub = $('input[name="messageSubject"]').val();
		var messBody = $('textarea[name="messageBody"]').val();
		var dataKnown = {
			'wallUserId_fk' : 'pikachu',
			'senderId_fk' : 'yoshi',
			'messageSubject' : messSub,
			'messageBody' : messBody
		};
		console.log(dataKnown);
		/* sentTimestamp dealt with in the database */
		$.ajax({
			url : "../sendMessage.cgi",
			type : "GET",
			data : dataKnown,
			success : function(data) {
				console.log("success");
				console.log(data);
				// You can additionally, have jQuery append to the document some success message (instead of an alert)
			}
		});
	});
});
