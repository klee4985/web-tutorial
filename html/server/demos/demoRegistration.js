$(document).ready(function() {
	$("form#exampleForm").submit(function(e) {
		// Make sure you suppress the default browser submit action, otherwise you will not be able to send the data
		e.preventDefault();

		// Extract out all the data in the form, and translate it into a GET method string to pass to the server
		var dataPass = $("form#exampleForm").serialize();
		// So you can see the data that is passed to the server, let's log it into the console
		console.log(dataPass);
		
		/*
		 url: is the url where you want to send the data
		 type: in this course, always use the GET method
		 data:
		 If you want to send custom data (i.e. no form extraction and submission, you must define your data)
		 i.e. dataPass = { name: "Charmander", location: "Volcana" }
		 success: if the url is successfully redirected to, what do you want to do? Maybe tell the user it was a success?

		 There are also other options, including "error" to handle the situation where the data cannot be submitted to the server

		 Of course, make sure you VALIDATE THE DATA FIRST!!!
		 */
		$.ajax({
			url : "../register.cgi",
			type : "GET",
			data : dataPass,
			success : function() {
				alert("You successfully created an account");
				// You can additionally, have jQuery append to the document some success message (instead of an alert)
			}
		});
	});
});
