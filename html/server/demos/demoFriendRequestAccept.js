$(document).ready(function() {
	$("button#request").click(function(e) {
		// Make sure you suppress the default browser submit action, otherwise you will not be able to send the data
		e.preventDefault();

		/* Assumes you already have found the user you want to request friendship from
		 and assumes you have kept track of who the current user is */
		var dataKnown = {
			'username' : 'yoshi',
			'friendname' : 'luigi'
		};
		/* sentTimestamp dealt with in the database */
		$.ajax({
			url : "../requestFriendship.cgi",
			type : "GET",
			data : dataKnown,
			success : function(data) {
				console.log("success");
				console.log(data);
				// You can additionally, have jQuery append to the document some success message (instead of an alert)
			}
		});
	});

	$("button#accept").click(function(e) {
		// Make sure you suppress the default browser submit action, otherwise you will not be able to send the data
		e.preventDefault();

		var dataKnown = {
			'username' : 'luigi',
			'friendname' : 'yoshi'
		};
		/* sentTimestamp dealt with in the database */
		$.ajax({
			url : "../acceptFriendship.cgi",
			type : "GET",
			data : dataKnown,
			success : function(data) {
				console.log("success");
				console.log(data);
				// You can additionally, have jQuery append to the document some success message (instead of an alert)
			}
		});
	});
});