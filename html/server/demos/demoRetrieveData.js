$(document).ready(function() {
	/*
	 Assuming you have already logged on and used some kind of web storage to retrieve username
	 */

	$.ajax({
		url : "../getFriends.cgi",
		type : "GET",
		data : {
			'username' : 'pikachu'
		},
		dataType : 'json',
		success : function(data) {
			console.log(data);
			/*
			The following is valid for getFriends.cgi data
			for(var i = 0; i < data.length; i++) {
				console.log("FRIEND: " + data[i].friend)
			}
			*/
			
			/* The following is valid for getUser.cgi data
			 for (var i = 0; i < data.length; i++) {
			 console.log("KEY: " + data[i].field + " VALUE: " + data[i].val);
			 }
			 
			 */

			/* For getMessageWall.cgi
			 console.log(data[0].wallUserId_fk);
			 */
		}
	});
});
