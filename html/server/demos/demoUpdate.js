$(document).ready(function() {
	$("form#exampleForm").submit(function(e) {
		e.preventDefault();

		// Assumes already know username
		var username = "username=mario&";
		// Notice that we pass in the username as well as the form data in GET format
		var dataPass = username + $("form#exampleForm").serialize();
		//console.log(dataPass);
		$.ajax({
			url : "../updateUser.cgi",
			type : "GET",
			data : dataPass,
			success : function() {
				console.log("success");
			}
		});
	});
});
